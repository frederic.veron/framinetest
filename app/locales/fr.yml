meta:
  title: Framinetest
  edu: <b class="orange"><i>Édu</i></b>
  lead: ' Le monde se divise en deux catégories : ceux qui ont un pistolet chargé
    et ceux qui creusent. Toi, tu creuses.'
nav:
  langChange: Changer la langue
  lang: Langue
  translate: Traduire
carousel:
  play: Jouer
  pause: Pause
  prev: Diapo précédente
  next: Diapo suivante
slides:
  download: Téléchargez Minetest pour vous ouvrir les portes du monde @:data.color.minetest
    @:meta.edu
  connect: Connectez-vous sur @:data.txt.minetest pour rejoindre notre monde dédié
    à l’éducation.
  create: Fabriquez des objets à partir de matières premières.
  learn: Utilisez le jeu pour apprendre et enseigner.
download:
  title: Télécharger
  warning: '<strong>ATTENTION</strong> : le logiciel que nous proposons pour Windows
    est configuré pour les claviers AZERTY. Pour les autres systèmes, le jeu est paramétré
    en QWERTY. Il vous faudra probablement configurer vos touches (onglet « <code>Réglages</code> »)'
  how: Afin de vous connecter sur un serveur Minetest (dont le nôtre) il vous faut
    télécharger un logiciel (un « client »). Il est disponible pour de nombreux systèmes
    d’exploitation.
  clients:
  - <a href="http://minetest.net/download#linux">GNU/Linux</a> (choisissez votre distribution)
  - 'Android: sur le <a href="https://play.google.com/store/apps/details?id=net.minetest.minetest">Google
    PlayStore</a> ou sur <a href="https://f-droid.org/repository/browse/?fdid=net.minetest.minetest">F-Droid</a>'
  - <a href="http://www.minetest.net/downloads/">Toutes les versions</a> (Windows, Mac OSX)
  keyboard: Configurer vos touches
  change: 'Il vous faut lancer le logiciel, cliquer sur l’onglet <code>Réglages</code>,
    puis sur le bouton <code>Changer les touches</code> en bas à droite :'
  config: 'Ensuite, configurez chacune de vos touches en cliquant sur celle que vous
    voulez modifier et en tapant la touche voulue. Voici notre modèle :'
play:
  title: Jouer
  warning: Comment lancer sa première partie, et tester le jeu quand on a quelques
    minutes devant soi ?
  game: Lancer une partie
  desc: 'Vous avez téléchargé Minetest et vous voulez jouer ? Au départ, deux solutions
    simples existent : jouer en mode solo sur votre machine ou rejoindre le monde
    de @:data.color.minetest @:meta.edu</>.'
  offline: En mode solo
  stepsOff:
  - Cliquez sur l'onglet "Solo"
  - Créez un "Nouveau monde" en remplissant la case "nom du monde" puis en cliquant
    sur "créer"
  - Cliquez sur ce monde puis sur le bouton "jouer"
  online: Sur le monde @:data.color.minetest @:meta.edu
  stepsOn:
  - Cliquez sur l'onglet "Client"
  - Choisissez le serveur "framinetest.org"
  - Entrez un nom (ou pseudonyme) puis un mot de passe et cliquez sur "jouer" !
  how: Comment jouer ?
  dontPanic: Le jeu utilisant votre souris, vous ne savez plus comment sortir ? Pas
    de panique ! Tapez sur Echap (menu), alt+tab (changer de programme) ou tout simplement
    "I" (inventaire de Minetest)
  actions:
  - <b>La souris</b> sert à orienter votre regard
  - '<b>Clic gauche</b> : miner, attaquer (dans l’inventaire : déplacer une pile d’objets)'
  - '<b>Clic droit</b> : placer des blocs, utiliser (dans l’inventaire : déplacer
    un objet, séparer)'
  - '<b>Molette</b> ou <b>0-9</b> : Sélectionner un objet dans la barre d’action.'
  - '<b>Z/Q/S/D</b> : se déplacer (haut, gauche, bas, droite)'
  - '<b>A</b> : lâcher ce qui est dans la main'
  - '<b>Majuscule</b> : décendre ou marcher avec prudence'
  - '<b>K</b> : voler (activer/désactiver le mode "déplacements libres")'
  - '<b>J</b> : activer/désactiver le mode "rapide"'
  - '<b>I</b> : ouvrir l’inventaire'
  - '<b>T</b> : ouvrir la fenêtre de discussion'
  - '<b>Echap</b> : ouvrir le menu (pour quitter, entre autres)'
  craft: 'Soyez constructifs : craftez !'
  more: Minetest n’est pas qu’un jeu qui casse des briques, c’est un monde où vous
    pouvez utiliser les matières premières récoltées pour fabriquer des objets (dans
    le jargon du jeu, on dit « <i>crafter</i> »). Découvrez comment et quels objets
    fabriquer <a href="http://wiki.minetest.net/Crafting/fr">dans le wiki de Minetest</a> !
classroom:
  title: Utiliser en classe
  warning: Dans ce jeu connu et apprécié des élèves, les possibilités d’enseignements
    (et d’activités pédagogiques) sont innombrables… Voici quelques exemples et suggestions.
  edu:
    title: 'Astuce : le monde réservé à l’éducation !'
    desc: Pour votre projet pédagogique, nous avons créé un monde spécialement réservé
      à l’éducation. Pour y accéder, allez sur l’onglet "Client", choisissez l’adresse
      "framinetest.org", et le Port "30000".
  create:
    title: Créer et construire ensemble
    text1: 'Apprendre à définir un (des) objectif(s) commun(s) puis se coordonner
      pour coopérer et collaborer pour le (les) atteindre sont essentiels dans le
      jeu. '
    text2: '… Quelques recommandations :'
    advices:
    - Il est souvent conseillé de débuter le travail coopératif/collaboratif en petits
      groupes (que vous pourrez étendre par la suite)
    - Le souvent, une répartition des rôles au sein des groupes de joueurs va se mettre
      en place. Mais si ce n’est pas le cas, n’hésitez pas à faciliter cette répartition.
    - Certains joueurs seront bien meilleurs que l’enseignant lui-même. C’est normal !
      N’hésitez pas à donner davantage de responsabilités aux joueurs les plus expérimentés.
    craft: 'Le crafting : fabriquez vos outils'
    text3: La <a href="http://wiki.minetest.net/Crafting/fr">fabrication – ou crafting
      en anglais —</a> est la méthode permettant la création de nouveaux <a href="http://wiki.minetest.net/Blocks/fr">blocs</a>
      (ou  d’<a href="http://wiki.minetest.net/Items/fr">objets</a>) dans le jeu.
      Ainsi, pour "crafter" quelque chose, le joueur devra ouvrir son inventaire (touche
      "i" du clavier), ouvrir sa table de craft (ou grille de fabrication) puis y
      disposer les blocs (ou objets) déjà à sa disposition selon un arrangement spécifique
      (que le joueur devra trouver par lui-même en sélectionnant, de manière logique,
      les bons matériaux).
    constructions: Les premières constructions… collaboratives ?
    text4: Comme pour l’ensemble des constructions du jeu, le choix des matériaux
      est une étape essentielle. Si ce dernier est mauvais, les édifices réalisés
      pourraient bien disparaître à cause des processus d’érosion ! Le même principe
      de réflexion doit être appliqué pour chaque objets/blocs, y compris les êtres
      vivants (animaux et végétaux).
  examples:
    title: Exemples d’activités pédagogiques
    desc: 'Voici quelques pistes d’exploitations pédagogiques possibles :'
    activities:
    - Vers une meilleure compréhension de l’intérêt pédagogique des « bacs à sable », l’exemple de Minestest / Minecraft <a href="http://www.dane.ac-versailles.fr/comprendre/vers-une-meilleure-comprehension-de-l-interet-pedagogique-des-bacs-a-sable"> sur le site de l'Académie de Versailles</a>.
    - '"Et si nous reproduisions notre collège à l’échelle ?" (Mathématiques, Technologies)'
    - '"Partons explorer une nouvelle planète !" ou "Étudions l’influence de l’Homme
      sur le peuplement d’un milieu" (Sciences de la vie et de la Terre)'
    - '"Reproduisons une bataille historique" (Histoire, Français)'
    - '"Imaginons et rêvons notre ville du futur !" (Géographie, SVT, Français)'
    - '"Construire un monde sans armes ?" (philo)'
  more:
    title: 'Aller plus loin : les ressources.'
    links:
    - Vers une meilleure compréhension de l’intérêt pédagogique des « bacs à sable », l’exemple de Minestest / Minecraft <a href="http://www.dane.ac-versailles.fr/comprendre/vers-une-meilleure-comprehension-de-l-interet-pedagogique-des-bacs-a-sable"> sur le site de l'Académie de Versailles</a>.
    - Les <a href="https://framablog.org/?p=6937">applications pédagogiques</a> de
      Framinetest Édu.
    - Rejoignez la communauté Framinetest-Minetest sur le forum des <a href="https://framacolibri.org/c/framinetest-minetest">Framacolibri</a>.  
    - Plus de ressources pédagogiques <a href="https://www.svtux.fr/svtux/minetest-accueil.html">sur
      le site de SVTux</a>.
    - Installez <a href="http://framacloud.org/?p=959">votre serveur Minetest</a> et plus en détail <a href="https://www.svtux.fr/svtux/svtuxboxproject/minetest.html">l'article de SVTux</a>.
    - Le <a href="http://www.minetest.net">site officiel de Minetest</a> [EN]
    - Le <a href="http://wiki.minetest.net/Main_Page/fr">wiki francophone de Minetest</a>
    - Le <a href="https://forum.minetest.net/viewforum.php?f=20&amp;sid=875d8cece94a28e7248b0cb18a006d82">forum
      francophone de Minetest</a>
what:
  title: Qu'est-ce que c'est ?
  subtitle: Explorez un monde numérique
  desc: 'Vous êtes un Robinson dans un jeu de légos vivant. Un monde fait de cubes :
    terre, eau, sable, minéraux, faune et flore… dont les règles qui le régissent
    simulent le nôtre (jour & nuit, température, érosion, reproduction des espèces…)'
  start: … Pourquoi ne pas commencer par l'explorer ?
  steps:
  - Présentez les règles du jeu à votre classe (voici <a href="/files/a-lire.pdf">un
    exemple</a>)
  - Accompagnez-les lors de leur première connexion.
  - Proposez leur de remplir une <a href="/files/Mission-Exploration-Planete-SVTux.pdf">Fiche
    d’explorateurs</a>
help:
  title: Prise en main
  desc: '@:data.color.minetest @:meta.edu est un monde du jeu « bac à sable » @:data.meta.src
    (monde de blocs à modifier) dédié à l’éducation, aux activités pédagogiques et
    au <i lang="en">serious gaming</i>.'
  start: 'Pour débuter :'
  steps:
  - Téléchargez le jeu ;
  - Connectez-vous à notre serveur ;
  - Jouez ! ;
  - Lisez <a href="http://wiki.minetest.net/Main_Page/fr">la documentation</a> pour
    en savoir plus.
software:
  title: Le logiciel
  basedon: '@:data.color.minetest @:meta.edu est une instance du serveur @:data.meta.src
    développé par la communauté minetest.'
  license: Minetest est sous licence @:data.license.ccbysa3</>.
garden:
  title: Cultivez votre jardin
  contrib: Pour participer au développement du logiciel, proposer des améliorations
    ou simplement le télécharger, rendez-vous sur
  devsite: le site de développement
  install: 'Si vous souhaitez installer ce logiciel pour votre propre usage et ainsi
    gagner en autonomie, nous vous aidons sur :'
  framacloud: https://framacloud.org/fr/cultiver-son-jardin/minetest.html
holidays:
  text1: 'C’est l’été, Framinetest fait une pause pour se mettre à jour et sera donc
    fermé quelques semaines. Profitez-en pour vous déconnecter un peu !'
  text2: 'Pour continuer à jouer, vous pouvez vous connecter à un autre serveur, créer
    le vôtre ou rester en local.'
