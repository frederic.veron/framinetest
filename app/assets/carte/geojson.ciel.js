var cielCollection = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Ile dans le ciel</b>, <br/><i>zorglub</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [0, 2000]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Deux petites îles dans le ciel</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [33, -118]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Gratte ciel</b>, <br/><i>talou</i><br/><img src='captures/GratteCielTalou.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-126, -87]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Ancien monde des licornes</b>, <br/><i>Simeon6C & Dorian6C</i><br/><img src='captures/AncienMondeLicorne.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-1200, -447]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Epée de Damocles</b>, <br/><i>talou</i><br/><img src='captures/EpeeDamocles.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-404, 56]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Sphere géante</b>, <br/><i>world edit</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [440, -503]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Mega ferme sphere</b>, <br/><i>Gordon</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [5, -491]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Etoile noire</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-622, -362]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Charlie</i><br/><img src='captures/MaisonCharlie.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [623, 498]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Vaisseau starwars</b>, <br/><i>??</i><br/><img src='captures/VaisseauStarWars.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-736, -211]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison dans les nuages</b>, <br/><i>Emma & Lucie</i><br/><img src='captures/MaisonEmma.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-7, -79]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Megasphere manuelle</b>, <br/><i>DooM</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-161, 52]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Bateau Zepplin</b>, <br/><i>khamelot</i><br/><img src='captures/BateauZeppelinKhamelot.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-283, -529]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Gélule aérienne</b>, <br/><i>Lilou</i><br/><img src='captures/GeluleLilou.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-140, -73]
      }
    }
  ]
};
