var soussolCollection = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Appartement dans une cage de verre</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -110, -174]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Station hydroélectrique</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -192, -191]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Labo</b>, <br/><i>DooM</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -208, -9]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Le fond du nether</b>, <br/><i>gene</i><br/><img src='captures/FondNether.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -26, 29]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Appartement en feuilles</b>, <br/><i>powi</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -596, -142]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Complexe sous-terrain</b>, <br/><i>Dwayn & qwerty</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -669, 515]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Entrée de la visite du volcan</b>, <br/><i>yannk</i><br/><img src='captures/EntreeVisiteVolcan.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -774, -71]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>trou pour descendre à la cave rouge</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -783, -325]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Caverne des lucioles</b>, <br/><i>yannk</i><br/><img src='captures/CaverneLucioles.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -788, -272]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Cité Minière</b>, <br/><i>RemyDePain</i><br/><img src='captures/MondeSousterrainRemi.jpg' style='width:200px;'/>",
        "marker": "circle",
        "color": "pink"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -140, -105]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Lounge sous-terrain</b>, <br/><i>ju</i><br/><img src='captures/LoungeJu.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -6, -255]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Guichets du Cinéma</b>, <br/><i>talou</i><br/><img src='captures/GuichetsCinemaTalou.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -11, -260]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Cinéma sous-terrain</b>, <br/><i>talou</i><br/><img src='captures/CinemaTalou.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ -33, -251]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Restaurant sous-terrain</b>, <br/><i>ju</i><br/><img src='captures/RestaurantJu.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ 32, -268]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Piscine municipale sous-terraine</b>, <br/><i>ju</i><br/><img src='captures/PiscineJu.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ 32, -229]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Vestiaires de la piscine</b>, <br/><i>ju</i><br/><img src='captures/VestiairesJu.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ 4, -249]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Cathédrale Saint-Tux</b>, <br/><i>ju</i><br/><img src='captures/CathedraleSaintTuxJu.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [ 19, -231]
      }
    }
  ]
};
